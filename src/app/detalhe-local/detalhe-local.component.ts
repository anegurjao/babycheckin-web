import { Component, OnInit, ViewChild } from '@angular/core';
import { LocaisService } from '../locais/locais.service';
import { DetalheLocalService } from './detalhe-local.service';
import { ActivatedRoute } from '../../../node_modules/@angular/router';
import {} from '@types/googlemaps';
import { Local } from '../model/local';

@Component({
  selector: 'app-detalhe-local',
  templateUrl: './detalhe-local.component.html',
  styleUrls: ['./detalhe-local.component.scss']
})
export class DetalheLocalComponent implements OnInit {

  @ViewChild('gmap') gmapElement: any;
  
  map: google.maps.Map;

  private localSelect: Local;
  caracteristicaLocal;
  avaliacoesLocal;

  idLocal;
  loading: boolean = true;
  loadingDetalhe: boolean = true;
  loadingAvaliacao: boolean = true;
  
  constructor(private routeActived: ActivatedRoute, private localService: LocaisService, private detalheService: DetalheLocalService) { 
    this.routeActived.params.subscribe( params => {
      this.idLocal = params['id'];
    });
  }

  ngOnInit() {
    this.localSelect = this.localService.getLocalSelecionado();
    this.detalheService.getDetalheLocal(this.idLocal).subscribe(response => {
      this.localSelect = response
      this.loadingDetalhe = false;
      this.setMapEndereco(this.localSelect.latitude, this.localSelect.longitude);
    });

    this.getCaracteristicaLocal();
    this.getAvaliacoesLocal();
  }

  getCaracteristicaLocal(){
    this.detalheService.getCaracteristicaLocal(this.idLocal).then(response => {
      this.caracteristicaLocal = response;
      this.loading = false;
    })
  }

  getAvaliacoesLocal(){
    this.detalheService.getAvaliacoesLocal(this.idLocal).then(response => {
      this.avaliacoesLocal = response;
      this.loadingAvaliacao = false;
    })
  }

  setMapEndereco(lat, long){
    var mapProp = {
      center: new google.maps.LatLng(lat, long),
      zoom: 17,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);

    //adicionar pin
    let marker =  new google.maps.Marker({
      position: new google.maps.LatLng(lat, long),
      map: this.map,
      title: ""
    });

    
  }


}
