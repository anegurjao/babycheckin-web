import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LocaisService } from '../locais/locais.service';
import { Observable } from 'rxjs/Observable';
import { Local } from '../model/local';

@Injectable()
export class DetalheLocalService {
    
    private url_api: String = "https://babycheckin-node.azurewebsites.net/api/v1/local/"

    constructor(private http: HttpClient, private localService: LocaisService) { }

    
    getDetalheLocal(id):Observable<Local> {
        return this.http.get(this.url_api + "/"  + id );
    }
    
    getCaracteristicaLocal(id) {
        return this.http.get(this.url_api + "caracteristica/"  + id ).toPromise();
    }

    getAvaliacoesLocal(id) {
        return this.http.get(this.url_api + "avaliacao/"  + id ).toPromise();
    }
    
}   