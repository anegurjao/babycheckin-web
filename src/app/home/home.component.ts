import { Component, OnInit } from '@angular/core';
import { ProfissionalService } from '../services/profissional.service';
import { LocaisService } from '../locais/locais.service';
import { Local } from '../model/local';
import { Router } from '@angular/router';
import { Cidade } from '../model/cidade';
import { CidadeService } from '../services/cidade.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  listaProfissionais: Array<Local>;
  cidades: Array<Cidade>;
  cidadeSelecionada: any = "onde";
  categoriaSelecionada: string = "";

  melhoresAvaliados: Array<Local>;
  loadingPlaceBetter: boolean = false;
  loadingProfessionals: boolean = false;
  loadingCidade: boolean = false;

  constructor(private profService: ProfissionalService, private locaisService: LocaisService, private cidadeService: CidadeService, private routeLink: Router) { }


  ngOnInit() {

    if (this.cidadeService.getCidadeCached() != null){
      this.cidades = this.cidadeService.getCidadeCached();
    }else{
      this.getCidadesDisponiveis();
    }

    this.getMelhoresAvaliados();

    this.loadingProfessionals = true;
    this.profService.getProfissionais().subscribe(response => {
      this.listaProfissionais =  response;
      this.loadingProfessionals = false;
    })
  }

  private getMelhoresAvaliados(){
    this.loadingPlaceBetter = true;
    this.locaisService.getMelhoresAvaliados().subscribe(response => {
      this.melhoresAvaliados = response;
      this.loadingPlaceBetter = false;
    })
  }

  private getCidadesDisponiveis(){
    this.loadingCidade = true;
    this.cidadeService.getCidades().subscribe(response => {
      this.cidades =  response;
      this.cidadeService.setCidadesCached(this.cidades);
      this.loadingCidade = false;
    })
  }

  public directDetalheLocal(profissional: Local){
    this.locaisService.setLocalSelecionado(profissional);
    this.routeLink.navigate(['/locais/detalhe-local', profissional.id])
  }

  setCidadeGlobal(){
    this.locaisService.setCidadeSelecionda(this.cidadeSelecionada);
    this.getMelhoresAvaliados();
  }

  pesquisaLocal(){
    if (this.categoriaSelecionada == ""){
      alert("Selecione o que está planejando!");
    }else if (this.cidadeSelecionada == "" || this.cidadeSelecionada == "onde" || this.cidadeSelecionada == null){
      alert("Selecione a cidade!");
      
    }
    else if (this.categoriaSelecionada == "restaurante"){
      this.routeLink.navigate(['/locais/restaurantes']);
    }else if (this.categoriaSelecionada == "entretenimento"){
      this.routeLink.navigate(['/locais/entretenimento']);
    }else if (this.categoriaSelecionada == "museus-teatros"){
      this.routeLink.navigate(['/locais/museus-teatros']);
    }else if (this.categoriaSelecionada == "pracas-parques"){
      this.routeLink.navigate(['/locais/pracas-parques']);
    }
  }
}
