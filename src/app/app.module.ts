import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng2ScrollableModule} from 'ng2-scrollable';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { LocaisComponent } from './locais/locais.component';
import { MenuComponent } from './menu/menu.component';
import { FooterComponent } from './footer/footer.component';
import { CidadeService } from './services/cidade.service';
import { HomeComponent } from './home/home.component';
import { LocaisService } from './locais/locais.service';
import { DetalheLocalComponent } from './detalhe-local/detalhe-local.component';
import { ProfissionalService } from './services/profissional.service';
import { DetalheLocalService } from './detalhe-local/detalhe-local.service';

const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'locais', component: LocaisComponent},
  {path: 'locais/:categoria', component: LocaisComponent},
  {path: 'locais/detalhe-local/:id', component: DetalheLocalComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    LocaisComponent,
    MenuComponent,
    FooterComponent,
    HomeComponent,
    DetalheLocalComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    Ng2ScrollableModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    ),
    // AgmCoreModule.forRoot({
    //   apiKey: 'AIzaSyAj7JjhJzNSxkTFc0Sq5jktsdI7qaosYPQ'
    // }),
    NgbModule.forRoot()
  ],
  providers: [LocaisService, ProfissionalService, DetalheLocalService, CidadeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
