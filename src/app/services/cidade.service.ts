import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Cidade } from '../model/cidade';

@Injectable()
export class CidadeService {
    
    private url_api: String = "https://babycheckin-node.azurewebsites.net/api/v1"
    private cidadesCached: Cidade[];
    
    constructor(private http: HttpClient) { }

    getCidades(): Observable<Cidade[]> {        
        return this.http.get<Cidade[]>(this.url_api + "/cidades");
    }

    getCidadeCached():Cidade[]{
        return this.cidadesCached;
    }

    setCidadesCached(cities: Cidade[]){
        this.cidadesCached = cities;
    }
}