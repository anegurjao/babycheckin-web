import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Local } from '../model/local';

@Injectable()
export class ProfissionalService {
    
    private url_api: String = "https://babycheckin-node.azurewebsites.net/api/v1"

    constructor(private http: HttpClient) { }

    getProfissionais(): Observable<Local[]> {
        return this.http.get<Local[]>(this.url_api + "/profissionais" );
      }
    
}