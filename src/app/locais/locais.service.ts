import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Local } from '../model/local';
import { Cidade } from '../model/cidade';
import { Observable } from 'rxjs';

@Injectable()
export class LocaisService {

  constructor(private http: HttpClient) { }

  private url_api: String = "https://babycheckin.azurewebsites.net/admin/index.php/app/services"
  private url_api_node: String = "https://babycheckin-node.azurewebsites.net/api/v1"
  
  private app_version: String = "site"

  private todosLocais    = {id_core_tipo: 0, id_core_tipo_plano:0 , referencia: 'todos'};
  private entretenimento = {id_core_tipo: 38, id_core_tipo_plano: 0, referencia: "todos"};  
  private restaurante    = {id_core_tipo: 7, id_core_tipo_plano: 0  , referencia: 'todos'};
  private museusTeatros  = {id_core_tipo: 49, id_core_tipo_plano: 0, referencia: "todos"};
  private pracasParques  = {id_core_tipo: 8, id_core_tipo_plano: 0, referencia: "todos"};

  private coordenada_SP = {latitude: Cidade.getSaoPaulo().latitude, longitude: Cidade.getSaoPaulo().longitude};
  private coordenadaCidadeSelecionada;

  private localSelecionado: Local;
  private cidadeSelecionada: Cidade = Cidade.getSaoPaulo();

  private getCoordenadaCidadeSelecionada(){
    if (this.coordenadaCidadeSelecionada == null){
      this.coordenadaCidadeSelecionada = this.coordenada_SP;
    }

    return this.coordenadaCidadeSelecionada;
  }

  public getCidadeSelecionada(): Cidade {
    return this.cidadeSelecionada;
  }

  public setCidadeSelecionda(city: Cidade){
    this.cidadeSelecionada = city;
    this.coordenadaCidadeSelecionada = {latitude: this.cidadeSelecionada.latitude, longitude: this.cidadeSelecionada.longitude};
  }

  public getLocalSelecionado(): Local{
    return this.localSelecionado;
  }

  public setLocalSelecionado(local: Local){
    this.localSelecionado = local;
  }

  getMelhoresAvaliados(): Observable<Local[]> {
    return this.http.get<Local[]>(this.url_api_node + "/local/ratebetter/sp");
  }

  getLocais() {
    return this.http.get(this.url_api + "?appversion="  + this.app_version 
                                      + "&coordenadas=" + JSON.stringify(this.getCoordenadaCidadeSelecionada()) 
                                      + "&dados=" + JSON.stringify(this.todosLocais) 
                                      + "&service=local_listar")
                    .toPromise();
  }

  getLocaisSaoPaulo() {
    return this.http.get(this.url_api + "?appversion="  + this.app_version 
                                      + "&coordenadas=" + JSON.stringify(this.coordenada_SP) 
                                      + "&dados=" + JSON.stringify(this.todosLocais) 
                                      + "&service=local_listar")
                    .toPromise();
  }

  getEntretenimento() {
    return this.http.get(this.url_api + "?appversion="  + this.app_version 
                                      + "&coordenadas=" + JSON.stringify(this.getCoordenadaCidadeSelecionada()) 
                                      + "&dados=" + JSON.stringify(this.entretenimento) 
                                      + "&service=local_listar")
                    .toPromise();
  }

  getRestaurante() {
    return this.http.get(this.url_api + "?appversion="  + this.app_version 
                                      + "&coordenadas=" + JSON.stringify(this.getCoordenadaCidadeSelecionada()) 
                                      + "&dados=" + JSON.stringify(this.restaurante) 
                                      + "&service=local_listar")
                    .toPromise();
  }

  getMuseusTeatros() {
    return this.http.get(this.url_api + "?appversion="  + this.app_version 
                                      + "&coordenadas=" + JSON.stringify(this.getCoordenadaCidadeSelecionada()) 
                                      + "&dados=" + JSON.stringify(this.museusTeatros) 
                                      + "&service=local_listar")
                    .toPromise();
  }

  getParquesPracas() {
    return this.http.get(this.url_api + "?appversion="  + this.app_version 
                                      + "&coordenadas=" + JSON.stringify(this.getCoordenadaCidadeSelecionada()) 
                                      + "&dados=" + JSON.stringify(this.pracasParques) 
                                      + "&service=local_listar")
                    .toPromise();
  }

  
}
