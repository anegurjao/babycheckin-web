import { Component, OnInit, ViewChild, Renderer2 } from '@angular/core';
import { LocaisService } from './locais.service';
import { ActivatedRoute, Router } from '@angular/router';
import { } from '@types/googlemaps';
import { Local } from '../model/local';
import { Cidade } from '../model/cidade';
import { CidadeService } from '../services/cidade.service';
// import { MarkerManager } from '@agm/core/services';
// import { AgmMarker } from '@agm/core'

@Component({
  selector: 'app-locais',
  templateUrl: './locais.component.html',
  styleUrls: ['./locais.component.scss']
})
export class LocaisComponent implements OnInit {

  @ViewChild('gmap') gmapElement: any;
  
  map: google.maps.Map;
  locaisExibir: any[] = new Array<any>();
  loading: boolean = false;
  categoriaRota: String;

  cidades: Array<Cidade>;
  cidadeSelecionada: any = "choose";
  
  constructor(private locaisService: LocaisService, private cidadeService: CidadeService ,private routeActived: ActivatedRoute, private routeLink: Router,private renderer: Renderer2) { 
    this.routeActived.params.subscribe( params => {
      this.categoriaRota = params['categoria'];
    });
  }

  ngOnInit() {
    if (this.cidadeService.getCidadeCached() != null){
      this.cidades = this.cidadeService.getCidadeCached();
      this.cidadeSelecionada = this.cidades.find(x => x.nome == this.locaisService.getCidadeSelecionada().nome);
    }else{
      this.cidadeService.getCidades().subscribe( response => {
        this.cidades = response;  
        this.cidadeSelecionada = this.cidades.find(x => x.nome == this.locaisService.getCidadeSelecionada().nome);
      });
    }

    this.loadPlaces();
  }

  setCidadeGlobal(){
    this.locaisService.setCidadeSelecionda(this.cidadeSelecionada);
    this.loadPlaces();
  }
  private loadPlaces(){
    this.locaisExibir = [];
    var mapProp = {
      center: new google.maps.LatLng(this.locaisService.getCidadeSelecionada().latitude, this.locaisService.getCidadeSelecionada().longitude),
      zoom: 13,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);
    
    this.loading = true;
    
    if (this.categoriaRota == "entretenimento"){
      this.locaisService.getEntretenimento().then( response => {
        this.stepAfterResponse(response["lista"]);
      })
    }else if (this.categoriaRota == "restaurantes"){
      this.locaisService.getRestaurante().then( response => {
        this.stepAfterResponse(response["lista"]);
      })
    }else if (this.categoriaRota == "museus-teatros"){
      this.locaisService.getMuseusTeatros().then( response => {
        this.stepAfterResponse(response["lista"]);
      })
    }else if (this.categoriaRota == "pracas-parques"){
      this.locaisService.getParquesPracas().then( response => {
        this.stepAfterResponse(response["lista"]);
      })
    }else{
      this.locaisService.getLocais().then( response => {
        this.stepAfterResponse(response["lista"]);
      })
    }
  }

  private stepAfterResponse(response){
    response.forEach(element => {
      this.locaisExibir.push(element)
    });
    this.loading = false;
    this.adicionarPins();
  }

  private adicionarPins(){

    this.locaisExibir.forEach(local => {
      let marker =  new google.maps.Marker({
        position: new google.maps.LatLng(local.latitude, local.longitude),
        map: this.map,
        title: local.local
      });

      this.attachTitlePin(marker, local.local, local.id_local, this.renderer)
      // marker.addListener('click', function() {
      //   //map.setZoom(8);
      //   //map.setCenter(marker.getPosition());
      // });

    });
  }

  private attachTitlePin(marker, titleLocal, id: string, render: Renderer2) {
    var infowindow = new google.maps.InfoWindow({
      content: titleLocal
    });

    marker.addListener('click', function() {
      infowindow.open(marker.get('map'), marker);

      document.getElementById(id).scrollIntoView( { 
        behavior: 'smooth', 
        block: 'start' 
      });
    });
  }

  public directDetalheLocal(local){
    this.locaisService.setLocalSelecionado(Local.parserLocalOld(local));
    this.routeLink.navigate(['/locais/detalhe-local', local.id_local])
  }

}
