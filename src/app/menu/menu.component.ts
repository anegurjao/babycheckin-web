import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  constructor(private router: Router) { }

  hiddenMenuMobile: boolean = true;

  clickItemMenu(section: string) {
    // this.router.navigate(['/'],
    // {fragment: section}
    // );

    this.clickMenuMobile();
  }

  clickMenuMobile(){
    this.hiddenMenuMobile = !this.hiddenMenuMobile;
    // if (this.hiddenMenuMobile == false){
    //   this.hiddenMenuMobile = true;
    // }else{
    //   this.hiddenMenuMobile = false;
    // }
  }

  ngOnInit() {
  }

}
