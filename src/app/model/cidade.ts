export class Cidade {
    nome: string;
    latitude: number;
    longitude: number;
    uf: string;

    constructor(private name:string, private lat: number, private long: number){
        this.nome = name;
        this.latitude = lat;
        this.longitude = long;
    }

    public static getSaoPaulo(): Cidade{
        return new Cidade("São Paulo", -23.54, -46.63);
    }

    public static getPOA(): Cidade{
        return new Cidade("Porto Alegre", -30.040321, -51.203175);
    }
}