export class Local {
    id?: number;
    local?: String;
    categoria?: String;
    latitude?:  number;
    longitude?: number;
    resumo?: number;
    foto?: string;
    telefone?: string;
    email?: string;
    site?: string;


    public static parserLocalOld(old): Local{
        var retorno: Local =  new Local();
        
        retorno.id = old["id_local"];
        retorno.local = old["local"];
        retorno.latitude = old["latitude"];
        retorno.longitude = old["longitude"];
        retorno.foto = old["fotocomurl"];

        return retorno;
    }
}